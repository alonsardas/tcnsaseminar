"""
Simulates LIF neurons population

Time units:     ms
Voltage units:  mV
"""
import matplotlib.axes
import numpy as np
import uncertainties


class LIFParams(object):
    def __init__(self, mu_ext, sigma, g_c, beta, N=2000, tau_m=20.0, V_r=10.0, V_th=20.0):
        if callable(mu_ext):
            self.mu_ext_func = mu_ext
        else:
            self.mu_ext_func = lambda t: mu_ext
        if callable(sigma):
            self.sigma_func = sigma
        else:
            self.sigma_func = lambda t: sigma
        self.g_c = g_c
        self.beta = beta
        self.N = N
        self.tau = tau_m * (1 - g_c)  # Next to Eq.8
        self.V_r = V_r
        self.V_th = V_th


class LIFSimulator(object):
    DEFAULT_TIME_STEP = 0.05

    def __init__(self, params: LIFParams):
        self.params = params
        self.random_generator = np.random.default_rng()

    def simulate(self, simulation_time: float, time_step=DEFAULT_TIME_STEP):
        """
        Simulation based on Eq.8
        """
        p = self.params

        time_steps = int(simulation_time / time_step)
        V_t = np.zeros((p.N, time_steps))
        is_firing = np.full(V_t.shape, False)
        ts = np.arange(0, time_steps) * time_step

        for t_index in range(1, time_steps):
            firing_neurons = V_t[:, t_index - 1] > p.V_th
            is_firing[:, t_index - 1] = firing_neurons
            # V_t[firing_neurons, t_index - 1] = p.V_r  # Resets neurons that fired last time

            # -V_{i}
            decay_term = -V_t[:, t_index - 1]

            # \frac{g_{c}}{N}\sum_{j\neq i}V_{j}
            connected_neurons = p.g_c / p.N * (np.sum(V_t[:, t_index - 1]) - V_t[:, t_index - 1])

            external_input = p.mu_ext_func(ts[t_index])

            # See Eq.2 for explanation
            # noise = p.sigma_func(ts[t_index]) * np.random.normal(size=p.N) * np.sqrt(p.tau / time_step)
            noise_sigma = p.sigma_func(ts[t_index]) * np.sqrt(p.tau) / np.sqrt(time_step)
            noise = self.random_generator.normal(scale=noise_sigma, size=p.N)

            dVdt = 1 / p.tau * (decay_term + connected_neurons + external_input + noise)

            V_t[:, t_index] = V_t[:, t_index - 1] + dVdt * time_step

            # \frac{\beta\tau}{N}\sum_{j}\sum_{n_{j}=-\infty}^{\infty}\delta\left(t-t_{n_{j}}\right)
            fired_pulse = p.beta / p.N * np.sum(firing_neurons)
            V_t[:, t_index] += fired_pulse

            V_t[firing_neurons, t_index] = p.V_r  # Reset neurons that fired last time

        return LIFSolution(self.params, ts, V_t, is_firing)


class LIFSolution(object):
    def __init__(self, params: LIFParams, ts: np.ndarray, V_t: np.ndarray, is_firing: np.ndarray):
        self.params = params
        self.ts = ts
        self.V_t = V_t
        self.is_firing = is_firing
        dt = self.ts[1] - self.ts[0]
        self.simulation_time = self.ts[-1] + dt

    def plot_spike_raster(self, ax: matplotlib.axes.Axes, t_range, num_of_neurons):
        relevant_time = np.logical_and(t_range[0] <= self.ts, self.ts <= t_range[1])
        relevant_firing = self.is_firing[0:num_of_neurons, relevant_time]
        # ax.imshow(relevant_firing, cmap='gray_r',
        #           extent=(self.ts[relevant_time][0], self.ts[relevant_time][-1], 1, num_of_neurons))
        # ax.pcolormesh(self.ts[relevant_time], np.arange(0, num_of_neurons), relevant_firing, shading='nearest', cmap='gray_r', interpo)

        relevant_neurons = np.arange(num_of_neurons)
        xs, ys = np.meshgrid(self.ts[relevant_time], relevant_neurons)
        relevant_xs = xs[relevant_firing]
        relevant_ys = ys[relevant_firing]
        ax.plot(relevant_xs, relevant_ys, '.', markersize=2.5)

        ax.set_title('Spike Raster')
        ax.set_xlabel('t (ms)')

    def calc_firing_rate(self, t_range, bin_size):
        if t_range[1] > self.simulation_time:
            raise ValueError(
                f'upper bound of t_range {t_range[1]} is bigger then the simulation time {self.simulation_time}')

        num_of_bins = int((t_range[1] - t_range[0]) / bin_size)
        firing_per_bin = np.zeros(num_of_bins)
        start_t = t_range[0]
        end_t = start_t + bin_size
        for i in range(num_of_bins):
            relevant_time = np.logical_and(start_t <= self.ts, self.ts < end_t)
            firing_per_bin[i] = np.sum(self.is_firing[:, relevant_time])
            start_t = end_t
            end_t += bin_size

        firing_rate = firing_per_bin / bin_size / self.params.N
        return firing_rate

    def plot_firing_rate(self, ax: matplotlib.axes.Axes, t_range, bin_size):
        firing_rate = self.calc_firing_rate(t_range, bin_size)
        ax.bar(np.arange(t_range[0], t_range[1], bin_size), firing_rate, width=1)
        ax.set_xlabel('t [ms]')
        ax.set_ylabel('firing rate [1/ms]')

    def plot_V_vs_t(self, ax: matplotlib.axes.Axes, neuron_index):
        ax.set_xlabel('t [ms]')
        ax.set_ylabel('V [mV]')
        return ax.plot(self.ts, self.V_t[neuron_index, :])

    def calc_average_firing_rate(self):
        # For each neuron, count haw many times it fired
        total_firing = self.is_firing.sum(axis=1)
        firing_rate: np.ndarray = total_firing / self.ts[-1]
        return uncertainties.ufloat(firing_rate.mean(), firing_rate.std())


def calc_autocorrelation(firing_rate):
    # See Eq. 33
    C = np.mean(firing_rate ** 2, axis=0)
    C /= np.mean(firing_rate) ** 2
    return C
