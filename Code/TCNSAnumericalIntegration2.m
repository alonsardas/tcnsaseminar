% TCNSA numerical integration - second try
figi = 0;

gc = 0.4;
beta = 8;
% x_vector = 17:0.2:19;
x_vector = 16:0.2:24;

nuResults = zeros(length(x_vector),1);
for k = 1:length(x_vector)
    % x = 18; % x = muExt/(1-gc);
    x = x_vector(k);
    muExt = x*(1-gc);
    
    sigma = 2;
    Vr = 10;
    Vth = 20;
    taum = 20; % taum = cm/gm;
    tau = taum/(1-gc);
    
    du = 0.01;
    dv = 0.01;
    dnu0 = 0.001;
    
    nu0 = 0:dnu0:0.15;
    muTot_vector = (muExt + tau*nu0*(beta - gc*(Vth-Vr)))/(1-gc);
    
    int = zeros(length(muTot_vector),1);
    for i = 1:length(muTot_vector)
        muTot = muTot_vector(i);
        
        % ATTENTION!!! opposite than the article
        lowerBound = (Vr - muTot)/sigma;
        upperBound = (Vth - muTot)/sigma;
        
        u_vector = lowerBound:du:upperBound;
        u_integrand = zeros(length(u_vector),1);
        for j = 1:length(u_vector)
            u = u_vector(j);
            v = (u-100):dv:u;
            v_integrand = exp(-v.^2);
            u_integrand(j) = exp(u.^2)*sum(v_integrand)*dv;
        end
        int(i) = 2*tau*sum(u_integrand)*du;
    end
    
    %% figs and results
    
    if figi == 1
        figure; plot(nu0,int,'-*'); grid on; xlabel('nu0'); ylabel('int');
        figure; plot(nu0,1./int,'-*'); grid on; xlabel('nu0'); ylabel('1./int');
        figure; plot(nu0,nu0,'-*',nu0,1./int,'-*'); grid on; xlabel('nu0');
        figure; plot(nu0,abs(nu0' - (1./int)),'-*'); grid on; xlabel('nu0'); ylabel('diff.');
    end
    
    [mini,indx] = min(abs(nu0' - (1./int)));
    nuResults(k) = nu0(indx);
end

% figure;
hold on; plot(x_vector,nuResults,'-.');
grid on; xlabel('x = \mu_{ext}/(1-gc)'); ylabel('\nu0');
% legend('\beta=4,gc=0','\beta=8,gc=0','\beta=4,gc=0.2','\beta=8,gc=0.2','\beta=4,gc=0.4','\beta=8,gc=0.4')
