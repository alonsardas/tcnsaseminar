import numpy as np
import matplotlib.pyplot as plt


class LIFParams(object):
    def __init__(self, g_c=0, beta=4, N=2000, tau_m=20.0*10**-3, V_r=10.0, V_th=20.0, miu_ext=18, sigma=2):
        self.sigma = sigma
        self.g_c = g_c
        self.beta = beta
        self.N = N
        self.tau = tau_m * (1 - g_c)  # Next to Eq.8
        self.V_r = V_r
        self.V_th = V_th
        self.miu_ext = miu_ext


class Simulator(object):
    def __init__(self, params: LIFParams, steps: int, v0_vector):
        self.params = params
        self.v0_vector = v0_vector
        self.steps = steps

    def gaussian_integral(self, u: float):
        lower = -10.0
        integral_value = 0.0
        v_vector = np.linspace(lower, u, self.steps)
        dv = (u - lower) / self.steps
        for v in v_vector:
            if np.abs(v) > 10.0:
                pass
            else:
                integral_value += dv * np.exp(-(v**2))
        return integral_value

    def complex_integral(self):
        p = self.params
        integral_vector = np.zeros(self.steps)
        for i in range(self.steps):
            v0 = self.v0_vector[i]
            miu_tot = (p.miu_ext + p.tau * v0 * (p.beta - p.g_c * (p.V_th - p.V_r))) / (1 - p.g_c)
            lower = (p.V_r - miu_tot) / p.sigma
            upper = (p.V_th - miu_tot) / p.sigma
            integral_value = 0
            u_vector = np.linspace(lower, upper, self.steps)
            du = (upper - lower) / self.steps
            print("miu " + str(miu_tot))
            print("lower " + str(lower))
            print("upper " + str(upper))
            for u in u_vector:
                gaussian_value = self.gaussian_integral(u)
                integral_value += du * np.exp(u**2) * gaussian_value
            integral_vector[i] = integral_value
            print("gaussian " + str(gaussian_value))
            print("value" + str(integral_value))
        return integral_vector

    def left_side(self):
        return 1/self.v0_vector

    def right_side(self):
        return 2 * self.params.tau * self.complex_integral()


steps = 100
ni0_vector = np.linspace(1, 200, steps)
model = Simulator(params=LIFParams(), steps=steps, v0_vector=ni0_vector)
plt.plot(ni0_vector, model.left_side())
plt.plot(ni0_vector, model.right_side())
plt.show()
