import matplotlib.axes
import matplotlib.figure
import numpy as np
from matplotlib import pyplot as plt

from LIFSimulator import LIFParams, LIFSimulator, calc_autocorrelation


def fig7() -> matplotlib.figure.Figure:
    # See params in Fig. 7
    g_c = 0.5
    beta = 2
    mu_ext = 11.5

    # sigma_A = 0.257
    sigma_A = 1
    simulation_time = 42e3
    # simulation_time = 3.2e3
    N = 1000
    time_step = 0.1
    total_points = 100
    sigma_func = lambda t: sigma_A * np.sin(np.pi * t / simulation_time)

    params = LIFParams(mu_ext, sigma_func, g_c, beta, N=N)
    simulator = LIFSimulator(params)
    sol = simulator.simulate(simulation_time, time_step)

    t_bins = np.linspace(0, simulation_time, total_points)
    C_vs_sigma = np.zeros(len(t_bins) - 1)
    sigmas = np.zeros(len(C_vs_sigma))
    for i in range(len(C_vs_sigma)):
        t = (t_bins[i] + t_bins[i + 1]) / 2
        sigmas[i] = sigma_func(t)
        firing_rate = sol.calc_firing_rate((t_bins[i], t_bins[i + 1]), 1)
        C = np.mean(calc_autocorrelation(firing_rate))
        C_vs_sigma[i] = C

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    ax.plot(sigmas, C_vs_sigma, '+-')
    ax.plot(sigmas[0], C_vs_sigma[0], '*')
    ax.set_ylabel('C(0)')
    ax.set_xlabel(r'$ \sigma\,\left[mV\right] $')

    # spike_raster_fig, ax = plt.subplots()
    # sol.plot_spike_raster(ax, (0, simulation_time), 100)
    return fig


def main():
    fig = fig7()
    fig.savefig(r'../Graphs/fig7.png')
    plt.show()


if __name__ == '__main__':
    main()
