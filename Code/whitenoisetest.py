import numpy as np
from matplotlib import pyplot as plt
from scipy import signal


def test():
    total_time = 1000
    bin_size = 0.4
    sigma = 1
    noise_sigma = sigma

    ts = np.arange(0, total_time, bin_size)
    I_noise = np.random.normal(scale=noise_sigma, size=ts.shape)

    fig, ax = plt.subplots()
    ax.plot(ts, I_noise)

    corr, lag = xcov(I_noise)
    fig, ax = plt.subplots()
    ax.plot(lag, corr)


def xcov(x: np.ndarray, y: np.ndarray = None, max_lag: int = -1):
    if y is None:
        y = x
    normalized_x = x - x.mean()
    normalized_y = y - y.mean()

    c = signal.correlate(normalized_x, normalized_y)
    lag = np.arange(-len(x) + 1, len(x))
    normalized_c = c / (len(x) - abs(lag))

    if max_lag >= 1:
        relevant = np.logical_and(-max_lag <= lag, lag <= max_lag)
        normalized_c = normalized_c[relevant]
        lag = lag[relevant]

    return normalized_c, lag


if __name__ == '__main__':
    test()
    plt.show()
