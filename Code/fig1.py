import matplotlib.figure
from matplotlib import pyplot as plt

from LIFSimulator import LIFParams, LIFSimulator


def fig1() -> matplotlib.figure.Figure:
    # See Fig.1 for parameters values

    sharey = False
    fig, axes = plt.subplots(1, 2, sharey=sharey)

    # excitatory
    gamma_gap = 0.26
    beta_gap = 5
    sol = calc_spiklet(gamma_gap, beta_gap)

    sol.plot_V_vs_t(axes[0], 1)
    # sol.plot_V_vs_t(axes[0], 0)
    axes[0].set_title('Dominantly excitatory spikelet')

    # Inhibitory
    gamma_gap = 0.33
    beta_gap = 2
    sol = calc_spiklet(gamma_gap, beta_gap)
    sol.plot_V_vs_t(axes[1], 1)
    axes[1].set_title('Dominantly inhibitory spikelet')
    return fig


def calc_spiklet(gamma_gap, beta_gap):
    # FIXME: In the graph in the article, the maximum voltage reached is about 0.5mV and the time
    #  is about 5ms. This is not what we get here

    time_step = 0.001

    pulse_time = 1
    has_shoot = False

    def mu_ext_func(t):
        nonlocal has_shoot
        if t < pulse_time:
            return 0
        if t >= pulse_time:
            if has_shoot:
                return 0
            else:
                has_shoot = True
                return [100 / time_step, 0]  # Excite the 1st neuron

    N = 2
    # See next to Eq.4
    gamma_c = gamma_gap * N
    beta = beta_gap * N

    # I think that g_m is not given anywhere, but this makes tau be sensible
    g_m = 0.08

    # After Eq.4
    g_c = gamma_c / (g_m + gamma_c)

    sigma = 0
    # It is not clear whether V_r should be +/-10
    # It is most likely to be in -10, because rest potential is at 0
    V_r = -10
    # V_r = 10
    V_th = 20
    params = LIFParams(mu_ext_func, sigma, g_c, beta, N=N, V_r=V_r, V_th=V_th)

    print(f'tau={params.tau}')

    simulator = LIFSimulator(params)
    return simulator.simulate(simulation_time=15, time_step=time_step)


def main():
    fig = fig1()
    fig.savefig(r'../Graphs/fig1.png')
    plt.show()


if __name__ == '__main__':
    main()
