import matplotlib.axes
import matplotlib.figure
import numpy as np
from matplotlib import pyplot as plt

from LIFSimulator import LIFParams, LIFSimulator, calc_autocorrelation


def fig4() -> matplotlib.figure.Figure:
    sigmas = np.arange(1.5, 2.01, 0.025)

    max_time = 1200
    t_range = (max_time - 200, max_time)
    time_step = 0.02

    N = 2000
    # For testing:
    # N = 600

    Cs = np.zeros(len(sigmas))
    for i, sigma in enumerate(sigmas):
        params = create_params(sigma, N)
        simulator = LIFSimulator(params)
        sol = simulator.simulate(max_time, time_step)
        firing_rate = sol.calc_firing_rate(t_range, 1)
        C = np.mean(calc_autocorrelation(firing_rate))
        print(f'sigma={sigma}, C={C}')
        Cs[i] = C
    # TODO: Save results!

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    ax.plot(sigmas, Cs, 'x-')
    ax.set_xlabel(r'$ \sigma\,\left[mV\right] $')
    ax.set_ylabel('C(0)')

    return fig


def create_params(sigma, N):
    mu_ext = 12
    g_c = 0.4
    V_r = 10

    beta = 5
    return LIFParams(mu_ext, sigma, g_c, beta, N=N, V_r=V_r)


def main():
    fig = fig4()
    fig.savefig(r'../Graphs/fig4.png')
    plt.show()


if __name__ == '__main__':
    main()
