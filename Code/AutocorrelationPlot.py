from matplotlib import pyplot as plt
import numpy as np
from LIFSimulator import LIFParams, LIFSimulator


def basic_simulation():
    # See Fig.4 for parameters values
    mu_ext_func = lambda t: 12
    g_c = 0.4
    # Fixme: The same behavior of transiting from synchronous to asynchronous is exhibited, but not on the same value
    #  of sigma. Maybe there is a problem with the noise?
    C_vector = []
    sigma_vector = np.linspace(1.5, 2, 10)
    for sigma in sigma_vector:
        firing_rate_vector = []
        for i in range(2):
            beta = 5
            params = LIFParams(mu_ext_func, sigma, g_c, beta, N=2000)

            simulator = LIFSimulator(params)
            sol = simulator.simulate(simulation_time=1200, time_step=0.05)
            fig, ax = plt.subplots()
            sol.plot_spike_raster(ax, (1000, 1200), 100)
            fig, ax = plt.subplots()
            firing_rate = sol.plot_firing_rate(ax, (1000, 1200), 1)
            firing_rate_vector.append(firing_rate)
        C = np.correlate(firing_rate_vector[0], firing_rate_vector[1], mode='full')
        C_vector.append(C[0])
    plt.plot(C_vector)
    plt.show()


basic_simulation()
