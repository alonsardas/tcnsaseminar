import numpy as np
from matplotlib import pyplot as plt

from LIFSimulator import LIFParams, LIFSimulator


def basic_simulation():
    # See Fig.4 for parameters values
    mu_ext = 12
    # mu_ext_func = lambda t: -10
    g_c = 0.4
    # Fixme: The same behavior of transiting from synchronous to asynchronous is exhibited, but not on the same value
    #  of sigma. Maybe there is a problem with the noise?
    test_sigma = 0.5
    synchronous_sigma = 1.8
    asynchronous_sigma = 1.85 + 0.3
    big_sigma = 4
    sigma = synchronous_sigma

    beta = 5
    # beta = 0.05
    # params = LIFParams(mu_ext_func, sigma, g_c, beta, V_r=-10)
    params = LIFParams(mu_ext, sigma, g_c, beta, V_r=10, N=600)

    simulator = LIFSimulator(params)
    sol = simulator.simulate(simulation_time=1200, time_step=0.01)

    fig, ax = plt.subplots()
    sol.plot_spike_raster(ax, (1000, 1200), 100)
    fig, ax = plt.subplots()
    sol.plot_firing_rate(ax, (1000, 1200), 1)

    fig, ax = plt.subplots()
    sol.plot_V_vs_t(ax, 0)


def test_noise0_beta0():
    """
    This shows that when other parameters are as described in the article, but
    sigma = beta = 0
    the behavior of neurons is to get close to V=20, but never reaching there
    """
    mu_ext_func = lambda t: 12
    g_c = 0.4
    sigma = 0
    # sigma = 1.8
    beta = 0
    params = LIFParams(mu_ext_func, sigma, g_c, beta)

    print(params.tau)

    simulator = LIFSimulator(params)
    simulation_time = 1200

    time_step = 0.03
    sol = simulator.simulate(simulation_time=simulation_time, time_step=time_step)

    fig, ax = plt.subplots()
    sol.plot_V_vs_t(ax, 0)
    firing = np.count_nonzero(sol.is_firing[0, :])
    firing_rate = firing / simulation_time
    print(f'firing rate: {firing_rate}')


def check_time_step_dependence():
    """
    Here we change the time step and check the how it changes the firing rate of a single neuron
    Ideally, since all time steps used are small enough (compared to tau), we would expect
    the firing rate to be constant.
    This is not the case however, probably because of bad implementation of the white noise
    """
    mu_ext = 12
    g_c = 0.4
    sigma = 1.8
    # sigma = 0.1
    beta = 0
    params = LIFParams(mu_ext, sigma, g_c, beta, N=400)

    simulator = LIFSimulator(params)
    simulation_time = 1200

    fig, ax = plt.subplots()

    dts = [0.02, 0.05, 0.1, 0.2, 0.5]
    for dt in dts:
        sol = simulator.simulate(simulation_time=simulation_time, time_step=dt)
        sol.plot_V_vs_t(ax, neuron_index=0)
        print(f'dt = {dt}')
        print(sol.calc_average_firing_rate())


def find_best_beta():
    mu_ext_func = lambda t: 12
    g_c = 0.4
    synchronous_sigma = 1.8
    asynchronous_sigma = 1.85

    # Between 0.235-0.24
    beta = 0.235
    synchronous_params = LIFParams(mu_ext_func, synchronous_sigma, g_c, beta)
    asynchronous_params = LIFParams(mu_ext_func, asynchronous_sigma, g_c, beta)

    max_time = 1200
    t_range = (max_time - 200, max_time)
    time_step = 0.02

    simulator = LIFSimulator(synchronous_params)
    synchronous_sol = simulator.simulate(simulation_time=max_time, time_step=time_step)
    simulator = LIFSimulator(asynchronous_params)
    asynchronous_sol = simulator.simulate(simulation_time=max_time, time_step=time_step)

    fig, axes = plt.subplots(1, 2)
    synchronous_sol.plot_firing_rate(axes[0], t_range, 1)
    asynchronous_sol.plot_firing_rate(axes[1], t_range, 1)

    fig, axes = plt.subplots(1, 2)
    synchronous_sol.plot_spike_raster(axes[0], t_range, 100)
    asynchronous_sol.plot_spike_raster(axes[1], t_range, 100)


def main():
    basic_simulation()
    # test_noise0_beta0()
    # check_time_step_dependence()
    # find_best_beta()
    # plt.show()


if __name__ == '__main__':
    main()
