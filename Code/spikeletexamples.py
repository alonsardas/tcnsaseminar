"""
Simulate spikelets for several values of beta and g_c
"""
import matplotlib.axes
import matplotlib.figure
import matplotlib.lines
from matplotlib import pyplot as plt

from LIFSimulator import LIFParams, LIFSimulator


def plot_beta_dependence() -> matplotlib.figure.Figure:
    g_c = 0.4
    betas = [0, 0.5, 1.5, 3]

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)

    for beta in betas:
        sol = calc_spiklet(g_c, beta)
        line: matplotlib.lines.Line2D = sol.plot_V_vs_t(ax, 1)[0]
        line.set_label(r'$ \beta $' + f'={beta}' + ' (mV)')
    ax.legend()
    ax.set_title(r'$ g_{c} $' + f'={g_c}' + ' fixed')

    return fig


def plot_g_c_dependence() -> matplotlib.figure.Figure:
    beta = 2
    g_cs = [0, 0.2, 0.5, 0.8]

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)

    for g_c in g_cs:
        sol = calc_spiklet(g_c, beta)
        line: matplotlib.lines.Line2D = sol.plot_V_vs_t(ax, 1)[0]
        line.set_label(r'$ g_{c} $' + f'={g_c}')
    ax.legend()
    ax.set_title(r'$ \beta $' + f'={beta}' + ' (mV)' + ' fixed')

    return fig


def calc_spiklet(g_c, beta):
    time_step = 0.001

    pulse_time = 2
    has_shoot = False

    def mu_ext_func(t):
        nonlocal has_shoot
        if t < pulse_time:
            return 0
        if t >= pulse_time:
            if has_shoot:
                return 0
            else:
                has_shoot = True
                return [5e2 / time_step, 0]  # Excite the 1st neuron

    N = 2
    sigma = 0

    # Note: In other simulation V_r=10, although fig1 uses V_r=-10
    V_r = -10
    V_th = 20
    params = LIFParams(mu_ext_func, sigma, g_c, beta, N=N, V_r=V_r, V_th=V_th)

    print(f'tau={params.tau}')
    simulation_time = 35

    simulator = LIFSimulator(params)
    return simulator.simulate(simulation_time=simulation_time, time_step=time_step)


def main():
    beta_dependence_fig = plot_beta_dependence()
    beta_dependence_fig.savefig(r'../Graphs/spiklet-examples-beta-dependence.png')
    g_c_dependence_fig = plot_g_c_dependence()
    g_c_dependence_fig.savefig(r'../Graphs/spiklet-examples-g_c-dependence.png')
    plt.show()


if __name__ == '__main__':
    main()
