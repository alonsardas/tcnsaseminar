import matplotlib.axes
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import least_squares
from scipy.special import erf

V_th = 20
V_r = 10
sigma = 2
tau_m = 20

should_plot_integral_func = False
should_plot_least_squares = False


def calc_v_0(x, g_c, beta):
    # After Eq.8
    tau = tau_m * (1 - g_c)

    def equation(v_0):
        # Eq. 23
        mu_tot = x + tau * v_0 * (beta - g_c * (V_th - V_r)) / (1 - g_c)

        if should_plot_integral_func:
            xs = np.linspace((V_r - mu_tot) / sigma, (V_th - mu_tot) / sigma, 200)
            ys = calc_integrand(xs)
            fig, ax = plt.subplots()
            ax.plot(xs, ys)
            plt.show()

        # print(f'integrating from {(V_r - mu_tot) / sigma} to {(V_th - mu_tot) / sigma}')
        # Since the value is 0 when u<-6, no point integrating that
        lower_bound = max((V_r - mu_tot) / sigma, -6)
        upper_bound = max((V_th - mu_tot) / sigma, -6)

        # integral1_value, _ = quad(calc_integrand, lower_bound, upper_bound)
        integral1_value = calc_integrated(upper_bound) - calc_integrated(lower_bound)
        # print(integral1_value)
        RHS = 2 * tau * integral1_value

        LHS = 1 / v_0

        # print(f'LHS: {LHS}\tRHS: {RHS}')
        return RHS - LHS

    # Based on the figure in the article
    initial_guess = 50 / (22 - 16) * (x - 16) * (beta / 2.4)
    initial_guess /= 1000

    sol = least_squares(lambda x: equation(x)**5, 0.001, bounds=(1e-4, 0.3), verbose=0)
    if should_plot_least_squares:
        xs = np.linspace(0.0001, 0.2, 200)
        func = np.vectorize(equation)
        ys = func(xs)
        fig, ax = plt.subplots()
        ax.plot(xs, ys)
        ax.plot(sol.x, func(sol.x), '*')
        print(f'found x={sol.x}')
        plt.show()

    return sol.x


class Data(object):
    def __init__(self, g_cs, betas, xs):
        self.g_cs = g_cs
        self.betas = betas
        self.xs = xs
        self.data = None
        self.linestyles = ['solid', 'dotted', 'dashed', (0, (5, 10)), 'dashdot']

    def calc_data(self):
        self.data = np.zeros((len(self.betas) * len(self.g_cs), len(self.xs)))
        for i, beta in enumerate(self.betas):
            for j, g_c in enumerate(self.g_cs):
                print(f'calculating for beta={beta},\tg_c={g_c}')
                for k, x in enumerate(self.xs):
                    value = calc_v_0(x, g_c, beta)
                    if np.isinf(value) or np.isnan(value):
                        print(f'Encountered an invalid value, x={x}')
                    self.data[i * len(self.g_cs) + j, k] = value
        self.data *= 1000  # Converting 1/ms -> Hz

    def save_data(self, path):
        np.savetxt(path, self.data)

    def load_data(self, path):
        self.data = np.loadtxt(path)

    def plot_all(self, ax: matplotlib.axes.Axes):
        for i, beta in enumerate(self.betas):
            for j, g_c in enumerate(self.g_cs):
                g_c_text = r'$ g_{c} $'
                label = f'{g_c_text}={g_c:.1f}'
                ax.plot(self.xs, self.data[i * len(self.g_cs) + j, :], linestyle=self.linestyles[j], label=label)

        ax.text(22, 70, rf'$ \beta $={self.betas[0]}')
        if len(self.betas) >= 2:
            ax.text(20.5, 160, rf'$ \beta $={self.betas[1]}')

        ax.set_xlabel(r'$ \mu_{ext}/\left(1-g_{c}\right)\,\left(mv\right) $')
        ax.set_ylabel(r'$ v_{0}\,\left(Hz\right) $')
        ax.legend()


def test():
    fig, ax = plt.subplots()
    assert (isinstance(ax, matplotlib.axes.Axes))
    xs = np.linspace(16.2, 25, 30)
    ys = np.zeros(xs.shape)
    for i, x in enumerate(xs):
        ys[i] = calc_v_0(x, 0.2, 4)

    ys *= 1000  # Converting 1/ms -> Hz

    ax.set_xlabel(r'$ \mu_{ext}/\left(1-g_{c}\right)\,\left(mv\right) $')
    ax.set_ylabel(r'$ v_{0}\,\left(Hz\right) $')

    print(ys)
    ax.plot(xs, ys, 'x')

    plt.show()


def tests():
    print(calc_v_0(22, 0.8, 8))
    g_c = 0.8
    x = 22
    beta = 8
    tau = tau_m * (1 - g_c)

    def equation(v_0):
        # Eq. 23
        mu_tot = x + tau * v_0 * (beta - g_c * (V_th - V_r)) / (1 - g_c)

        def integrand(u):
            integral2_value = np.sqrt(np.pi) / 2 * erf(u) + np.sqrt(np.pi) / 2
            # print(f'inner_integral: {integral2_value}')
            val = np.exp(u ** 2) * integral2_value
            val[np.isnan(val)] = 0
            return val

        xs = np.linspace((V_r - mu_tot) / sigma, (V_th - mu_tot) / sigma, 500)
        ys = integrand(xs)
        fig, ax = plt.subplots()
        ax.plot(xs, ys)

    equation(0.001)
    plt.show()
    print(calc_v_0(17, 0, 8))


def fig2():
    g_cs = np.arange(0, 0.9, 0.2)
    betas = [4, 8]
    xs = np.linspace(16.02, 23, 100)

    data_path = r'../Data/fig2-data.txt'
    data = Data(g_cs, betas, xs)
    data.calc_data()
    data.save_data(data_path)
    data.load_data(data_path)
    fig, ax = plt.subplots()
    data.plot_all(ax)


def simple_fig2():
    g_cs = np.arange(0.2, 0.9, 0.2)
    betas = [4, 8]
    xs = np.linspace(16.02, 21, 100)

    data_path = r'../Data/fig2-data.txt'
    data = Data(g_cs, betas, xs)
    data.calc_data()
    data.save_data(data_path)
    data.load_data(data_path)
    fig, ax = plt.subplots()
    data.plot_all(ax)


def calc_integrand(u):
    global integrand_func
    if integrand_func is None:
        integrand_func = _create_integrand_func()
    return integrand_func(u)


def _create_integrand_func():
    xs = np.linspace(-20, 3, 2000)

    func = np.vectorize(_integrand_func)
    ys = func(xs)

    return interp1d(xs, ys, kind='linear')


def _integrand_func(u):
    if u < -6:
        return 0.0

    """
    def inner_integrand(v):
        return np.exp(-v ** 2)

    integral2_value_old, _ = quad(inner_integrand, -np.inf, u)
    """

    integral2_value = np.sqrt(np.pi) / 2 * (erf(u) + 1)
    return np.exp(u ** 2) * integral2_value


integrand_func = None

integrated_func = None


def calc_integrated(w):
    global integrated_func
    if integrated_func is None:
        integrated_func = _create_integrated_func()
    return integrated_func(w)


def _create_integrated_func():
    xs = np.linspace(-6, 6, 40000)
    ys = np.zeros(xs.shape)

    dx = xs[1] - xs[0]

    for i in range(0, len(xs) - 1):
        left_x, right_x = xs[i], xs[i + 1]
        left_y, right_y = _integrand_func(right_x), _integrand_func(left_x)
        trapezoid_area = (left_y + right_y) / 2 * dx
        ys[i + 1] = ys[i] + trapezoid_area

    return interp1d(xs, ys, kind='linear')


def main():
    simple_fig2()
    plt.show()


if __name__ == '__main__':
    main()
    # tests()
