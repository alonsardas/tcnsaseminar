import matplotlib.figure
from matplotlib import pyplot as plt

from LIFSimulator import LIFParams, LIFSimulator


def fig5():
    mu_ext = 12
    g_c = 0.4
    synchronous_sigma = 1.8
    asynchronous_sigma = 1.85
    asynchronous_sigma += 0.01

    beta = 5
    synchronous_params = LIFParams(mu_ext, synchronous_sigma, g_c, beta)
    asynchronous_params = LIFParams(mu_ext, asynchronous_sigma, g_c, beta)

    max_time = 1200
    t_range = (max_time - 200, max_time)
    time_step = 0.02

    simulator = LIFSimulator(synchronous_params)
    synchronous_sol = simulator.simulate(simulation_time=max_time, time_step=time_step)
    simulator = LIFSimulator(asynchronous_params)
    asynchronous_sol = simulator.simulate(simulation_time=max_time, time_step=time_step)

    firing_rate_fig, axes = plt.subplots(1, 2)
    synchronous_sol.plot_firing_rate(axes[0], t_range, 1)
    asynchronous_sol.plot_firing_rate(axes[1], t_range, 1)

    assert isinstance(firing_rate_fig, matplotlib.figure.Figure)
    firing_rate_fig.suptitle('Firing Rate')
    axes[0].set_ylim(0, 0.8)
    axes[1].set_ylim(0, 0.8)
    axes[0].set_title('synchronous state')
    axes[1].set_title('asynchronous state')
    axes[1].label_outer()

    spike_raster_fig, axes = plt.subplots(1, 2)
    synchronous_sol.plot_spike_raster(axes[0], t_range, 100)
    asynchronous_sol.plot_spike_raster(axes[1], t_range, 100)

    spike_raster_fig.suptitle('Spike Raster')
    axes[0].set_title('synchronous state')
    axes[1].set_title('asynchronous state')

    return firing_rate_fig, spike_raster_fig


def main():
    firing_rate_fig, spike_raster_fig = fig5()
    firing_rate_fig.savefig(r'../Graphs/fig5-firing-rate.png')
    spike_raster_fig.savefig(r'../Graphs/fig5-spike-raster.png')

    plt.show()


if __name__ == '__main__':
    main()
