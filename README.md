
## 76908TCNSA - Final Project

This is the final project for 76908TCNSA, 2020.
As part of the project, we need to replicate the graphs in a given article, and explain the results.

We are working on the article:

[Ostojic, Srdjan & Brunel, Nicolas & Hakim, Vincent. (2008). Synchronization properties of networks of electrically coupled neurons in the presence of noise and heterogeneities. Journal of computational neuroscience. 26. 369-92. 10.1007/s10827-008-0117-3.](https://www.researchgate.net/publication/23498456_Synchronization_properties_of_networks_of_electrically_coupled_neurons_in_the_presence_of_noise_and_heterogeneities)

---

